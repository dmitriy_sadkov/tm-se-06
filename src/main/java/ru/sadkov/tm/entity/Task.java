package ru.sadkov.tm.entity;

import java.util.Date;

public class Task {
    private String name;
    private String id;
    private String description;
    private Date dateBegin;
    private Date dateEnd;
    private String projectId;
    private String userId;

    public Task(String name, String id, String projectId, String userId) {
        this.name = name;
        this.id = id;
        this.projectId = projectId;
        this.userId = userId;
    }

    public Task() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TASK NAME: " + name + "\n");
        sb.append("TASK ID: " + id + "\n");
        sb.append("PROJECT ID: " + projectId + "\n");
        sb.append("DESCRIPTION: " + description + "\n");
        sb.append("DATE BEGIN: " + dateBegin + "\n");
        sb.append("DATE END: " + dateEnd + "\n\n");
        return sb.toString();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
