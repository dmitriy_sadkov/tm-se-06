package ru.sadkov.tm.enumeration;

public enum Role {
    USER("user"),
    ADMIN("admin");

    private String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String displayName(){
        return displayName;
    }
}
