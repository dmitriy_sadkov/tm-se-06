package ru.sadkov.tm.repository;

import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;

import java.util.*;

public class ProjectRepository {
    private Map<String, Project> projectMap = new LinkedHashMap<>();

    public boolean isEmpty() {
        return projectMap.isEmpty();
    }

    public Map<String, Project> findAll(User user) {
        Map<String, Project> result = new LinkedHashMap<>();
        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (entry.getValue().getUserId().equals(user.getId())) {
                result.put(entry.getKey(), entry.getValue());
            }
        }
        return result;
    }


    public void persist(Project project) {
        projectMap.put(project.getId(), project);
    }

    public void remove(String projectName) {
        projectMap.values().removeIf(nextProject -> nextProject.getName().equalsIgnoreCase(projectName));
    }

    public void removeAll(User currentUser) {
        projectMap.values().removeIf(nextProject -> nextProject.getUserId().equals(currentUser.getId()));
    }

    public Project findOne(String projectId, User currentUser) {
        return findAll(currentUser).get(projectId);
    }

    public void merge(Project project) {
        if (projectMap.containsValue(project)) {
            update(project,project.getDescription(), project.getName());
        } else {
            persist(project);
        }
    }

    public void update(Project project, String description, String projectName) {
        project.setName(projectName);
        project.setDescription(description);
    }
}
