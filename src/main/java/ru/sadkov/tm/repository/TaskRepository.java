package ru.sadkov.tm.repository;

import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;

import java.util.*;

public class TaskRepository {
    private Map<String, Task> taskList = new LinkedHashMap<>();

    public boolean isEmpty() {
        return taskList.isEmpty();
    }

    public Map<String, Task> findAll(User currentUser) {
        Map<String,Task> result = new LinkedHashMap<>();
        for (Map.Entry<String,Task> entry: taskList.entrySet()) {
            if(entry.getValue().getUserId().equals(currentUser.getId())){
                result.put(entry.getKey(),entry.getValue());
            }
        }
        return result;
    }

    public void persist(Task task) {
        taskList.put(task.getId(),task);
    }

    public void remove(String taskName) {
        taskList.values().removeIf(nextTask -> nextTask.getName().equalsIgnoreCase(taskName));
    }

    public void removeAll(User user) {
        taskList.values().removeIf(nextTask -> nextTask.getUserId().equals(user.getId()));
    }

    public Task findOne(String taskId) {
        return taskList.get(taskId);
    }

    public void merge(Task task) {
        if (taskList.containsValue(task)) {
            update(task, task.getName());
        } else {
            persist(task);
        }
    }

    public void update(Task task, String taskName) {
        task.setName(taskName);
    }

    public Task findTaskByName(String taskName, User currentUser) {
        for (Task task: findAll(currentUser).values()){
            if(taskName.equals(task.getName())){
               return task;
            }
        }
        return null;
    }
}
