package ru.sadkov.tm.repository;

import ru.sadkov.tm.entity.User;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class UserRepository {
    private Map<String, User> userMap = new LinkedHashMap<>();


    public void persist(User user) {
        userMap.put(user.getId(), user);
    }

    public User findOne(final String userId) {
        return userMap.get(userId);
    }

    public User findByLogin(final String login) {
        List<User> userList = new ArrayList<>(userMap.values());
        for (User user : userList) {
            if (user.getLogin().equals(login))
                return user;
        }
        return null;
    }

    public List<User> findAll() {
        List<User> userList = new ArrayList<>(userMap.values());
        return userList;
    }

    public void removeOne(final String id) {
        userMap.remove(id);
    }

    public void removeAll() {
        userMap.clear();
    }

    public void update(String userId, String login) {
        userMap.get(userId).setLogin(login);
    }

}
