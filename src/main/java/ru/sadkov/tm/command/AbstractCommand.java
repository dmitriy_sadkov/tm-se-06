package ru.sadkov.tm.command;

import ru.sadkov.tm.launcher.Bootstrap;

public abstract class AbstractCommand {
    protected Bootstrap bootstrap;

    public AbstractCommand() {
    }

    public AbstractCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public Bootstrap getBootstrap() {
        return bootstrap;
    }

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;

    public abstract boolean safe();
}
