package ru.sadkov.tm.command.project;

import ru.sadkov.tm.command.AbstractCommand;

public class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER NAME:]");
        String projectName = bootstrap.getScanner().nextLine();
        if (projectName == null || projectName.isEmpty()) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        bootstrap.getProjectService().saveProject(projectName,bootstrap.getUserService().getCurrentUser().getId());
    }

    @Override
    public boolean safe() {
        return false;
    }
}
