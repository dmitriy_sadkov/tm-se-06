package ru.sadkov.tm.command.project;

import ru.sadkov.tm.command.AbstractCommand;

public class ProjectRemoveAllCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECTS CLEAR]");
        bootstrap.getProjectService().clearProjects(bootstrap.getUserService().getCurrentUser());
        bootstrap.getTaskService().clearTasks(bootstrap.getUserService().getCurrentUser());
    }

    @Override
    public boolean safe() {
        return false;
    }
}
