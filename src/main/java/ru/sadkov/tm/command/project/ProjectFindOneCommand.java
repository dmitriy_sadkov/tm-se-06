package ru.sadkov.tm.command.project;

import ru.sadkov.tm.command.AbstractCommand;

public class ProjectFindOneCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-show";
    }

    @Override
    public String description() {
        return "Show project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        String projectName = bootstrap.getScanner().nextLine();
        if (projectName == null || projectName.isEmpty() || bootstrap.getUserService().getCurrentUser() == null) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        bootstrap.getProjectService().showProject(projectName,bootstrap.getUserService().getCurrentUser());
    }

    @Override
    public boolean safe() {
        return false;
    }
}
