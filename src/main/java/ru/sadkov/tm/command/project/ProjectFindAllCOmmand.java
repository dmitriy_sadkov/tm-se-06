package ru.sadkov.tm.command.project;

import ru.sadkov.tm.command.AbstractCommand;

public class ProjectFindAllCOmmand extends AbstractCommand {
    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECTS:]");
        bootstrap.getProjectService().showProjectsForUser(bootstrap.getUserService().getCurrentUser());
    }

    @Override
    public boolean safe() {
        return false;
    }
}
