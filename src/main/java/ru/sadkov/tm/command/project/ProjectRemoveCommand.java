package ru.sadkov.tm.command.project;

import ru.sadkov.tm.command.AbstractCommand;

public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String description() {
        return "Remove selected project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("[ENTER NAME:]");
        String projectName = bootstrap.getScanner().nextLine();
        if (projectName == null || projectName.isEmpty() || bootstrap.getUserService().getCurrentUser() == null) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        bootstrap.getTaskService().removeTaskForProject(projectName,bootstrap.getUserService().getCurrentUser());
        bootstrap.getProjectService().removeProject(projectName,bootstrap.getUserService().getCurrentUser());
    }

    @Override
    public boolean safe() {
        return false;
    }
}
