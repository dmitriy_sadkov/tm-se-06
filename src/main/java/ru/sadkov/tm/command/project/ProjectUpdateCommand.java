package ru.sadkov.tm.command.project;

import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.Project;

public class ProjectUpdateCommand extends AbstractCommand {


    @Override
    public String command() {
        return "project-update";
    }

    @Override
    public String description() {
        return "Update project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        String projectName = bootstrap.getScanner().nextLine();
        if (projectName == null || projectName.isEmpty() || bootstrap.getUserService().getCurrentUser() == null) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        String projectId = bootstrap.getProjectService().findProjectIdByName(projectName,bootstrap.getUserService().getCurrentUser());
        if(projectId==null||projectId.isEmpty()) return;
        Project project = bootstrap.getProjectService().getProjectRepository().findOne(projectId,bootstrap.getUserService().getCurrentUser());
        if (project == null) {
            System.out.println("[ACCESS DENIED]");
            return;
        }
        System.out.println("[ENTER NEW NAME]");
        String newName = bootstrap.getScanner().nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        String description = bootstrap.getScanner().nextLine();
        bootstrap.getProjectService().update(project,newName,description);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
