package ru.sadkov.tm.command.user;

import ru.sadkov.tm.command.AbstractCommand;

public class UserEndSessionCommand extends AbstractCommand {
    @Override
    public String command() {
        return "logout";
    }

    @Override
    public String description() {
        return "End user session";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGOUT]");
        bootstrap.getUserService().userLogout();
        System.out.println("[OK]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
