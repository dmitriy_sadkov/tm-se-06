package ru.sadkov.tm.command.user;

import ru.sadkov.tm.command.AbstractCommand;

public class UserAuthorizationCommand extends AbstractCommand {
    @Override
    public String command() {
        return "login";
    }

    @Override
    public String description() {
        return "Authorize user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[AUTHORIZATION]");
        System.out.println("[ENTER LOGIN]");
        String login = bootstrap.getScanner().nextLine();
        System.out.println("[ENTER PASSWORD]");
        String password = bootstrap.getScanner().nextLine();
        if (bootstrap.getUserService().userAuthorize(login, password)) {
            System.out.println("[YOU ARE SUCCESSFULLY LOG IN]");
            return;
        }
        System.out.println("[SOMETHING WRONG! TRY AGAIN]");

    }

    @Override
    public boolean safe() {
        return true;
    }
}
