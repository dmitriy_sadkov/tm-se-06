package ru.sadkov.tm.command.user;

import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.User;

public class UserShowProfileCommand extends AbstractCommand {
    @Override
    public String command() {
        return "show-user";
    }

    @Override
    public String description() {
        return "Show current User profile";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CURRENT USER]");
        if(bootstrap.getUserService().getCurrentUser()==null){
            System.out.println("[NO USER! PLEASE LOGIN]");
            return;
        }
        User currentUser = bootstrap.getUserService().showUser();
        System.out.println(currentUser);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
