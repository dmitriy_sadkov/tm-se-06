package ru.sadkov.tm.command.user;

import ru.sadkov.tm.command.AbstractCommand;

public class UserUpdateInformationCommand extends AbstractCommand {
    @Override
    public String command() {
        return "update-profile";
    }

    @Override
    public String description() {
        return "Updating User profile";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATING PROFILE]");
        if(bootstrap.getUserService().getCurrentUser()==null){
            System.out.println("[NO USER! PLEASE LOGIN]");
            return;
        }
        System.out.println("[ENTER NEW LOGIN]");
        String newUserName = bootstrap.getScanner().nextLine();
        if(bootstrap.getUserService().updateProfile(newUserName)){
            System.out.println("[SUCCESS]");
        }else {
            System.out.println("[SOMETHING WRONG! TRY AGAIN]");
        }
    }

    @Override
    public boolean safe() {
        return false;
    }
}
