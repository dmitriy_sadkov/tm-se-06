package ru.sadkov.tm.command.user;

import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.util.HashUtil;

public class UserRegistryCommand extends AbstractCommand {
    @Override
    public String command() {
        return "user-register";
    }

    @Override
    public String description() {
        return "Register new user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REGISTRATION]");
        System.out.println("ENTER LOGIN");
        String login = bootstrap.getScanner().nextLine();
        System.out.println("[ENTER PASSWORD]");
        String password = bootstrap.getScanner().nextLine();
        User user = new User(login, HashUtil.hashMD5(password), Role.USER);
        bootstrap.getUserService().userRegister(user);
        System.out.println("[USER ADDED]");
    }

    @Override
    public boolean safe() {
        return true;
    }
}
