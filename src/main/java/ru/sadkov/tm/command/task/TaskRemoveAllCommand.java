package ru.sadkov.tm.command.task;

import ru.sadkov.tm.command.AbstractCommand;

public class TaskRemoveAllCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASKS CLEAR]");
        bootstrap.getTaskService().clearTasks(bootstrap.getUserService().getCurrentUser());
    }

    @Override
    public boolean safe() {
        return false;
    }
}
