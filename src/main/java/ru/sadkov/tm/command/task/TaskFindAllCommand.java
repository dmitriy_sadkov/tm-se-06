package ru.sadkov.tm.command.task;

import ru.sadkov.tm.command.AbstractCommand;

public class TaskFindAllCommand extends AbstractCommand {
    @Override
    public String command() {
        return "show-all-task";
    }

    @Override
    public String description() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASKS:]");
        bootstrap.getTaskService().showTasks(bootstrap.getUserService().getCurrentUser());
    }

    @Override
    public boolean safe() {
        return false;
    }
}
