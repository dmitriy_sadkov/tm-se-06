package ru.sadkov.tm.command.task;

import ru.sadkov.tm.command.AbstractCommand;

public class TaskFindOneCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-show";
    }

    @Override
    public String description() {
        return "Show task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER TASK NAME]");
        String taskName = bootstrap.getScanner().nextLine();
        if (taskName == null || taskName.isEmpty() || bootstrap.getUserService().getCurrentUser() == null) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        bootstrap.getTaskService().showTask(taskName,bootstrap.getUserService().getCurrentUser());
    }

    @Override
    public boolean safe() {
        return false;
    }
}
