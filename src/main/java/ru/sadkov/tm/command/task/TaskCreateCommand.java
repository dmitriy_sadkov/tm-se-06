package ru.sadkov.tm.command.task;

import ru.sadkov.tm.command.AbstractCommand;

public class TaskCreateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER NAME:]");
        String taskName = bootstrap.getScanner().nextLine();
        System.out.println("[ENTER PROJECT NAME]");
        String projectName = bootstrap.getScanner().nextLine();
        if (taskName == null || taskName.isEmpty() || projectName == null || projectName.isEmpty()) {
            System.out.println("[INCORRECT NAME]");
            return;
        }

        bootstrap.getTaskService().saveTask(taskName, projectName,bootstrap.getUserService().getCurrentUser());
    }

    @Override
    public boolean safe() {
        return false;
    }
}
