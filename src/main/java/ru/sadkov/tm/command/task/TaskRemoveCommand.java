package ru.sadkov.tm.command.task;

import ru.sadkov.tm.command.AbstractCommand;

public class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "Remove task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK REMOVE]");
        System.out.println("[ENTER NAME:]");
        String taskName = bootstrap.getScanner().nextLine();
        if (taskName == null || taskName.isEmpty() || bootstrap.getUserService().getCurrentUser() == null) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        bootstrap.getTaskService().removeTask(taskName,bootstrap.getUserService().getCurrentUser());
        System.out.println("[OK]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
