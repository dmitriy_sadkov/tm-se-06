package ru.sadkov.tm.command.task;

import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.ArrayList;
import java.util.List;

public class TaskForProjectCommand extends AbstractCommand {
    @Override
    public String command() {
        return "tasks-for-project";
    }

    @Override
    public String description() {
        return "Show tasks for chosen project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASKS FOR PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        String projectName = bootstrap.getScanner().nextLine();
        if (projectName == null || projectName.isEmpty()) {
            System.out.println("[INVALID PROJECT NAME]");
            return;
        }
        String projectId = bootstrap.getProjectService().findProjectIdByName(projectName, bootstrap.getUserService().getCurrentUser());
        if (projectId == null || projectId.isEmpty()) {
            System.out.println("[INVALID PROJECT NAME]");
            return;
        }
        List<Task> result = new ArrayList<>();
        for (Task task : bootstrap.getTaskService().getTaskRepository().findAll(bootstrap.getUserService().getCurrentUser()).values()) {
            if (task.getProjectId() != null && task.getProjectId().equals(projectId)) {
                result.add(task);
            }
        }
        if (result.isEmpty()) {
            System.out.println("[NO TASKS IN PROJECT]");
        } else {
            ListShowUtil.showList(result);
        }
    }

    @Override
    public boolean safe() {
        return false;
    }
}
