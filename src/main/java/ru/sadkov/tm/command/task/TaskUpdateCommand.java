package ru.sadkov.tm.command.task;

import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.Task;

public class TaskUpdateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-update";
    }

    @Override
    public String description() {
        return "Update task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER TASK NAME]");
        String taskName = bootstrap.getScanner().nextLine();
        if (taskName == null || taskName.isEmpty() || bootstrap.getUserService().getCurrentUser() == null) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        Task task = bootstrap.getTaskService().findTaskByName(taskName, bootstrap.getUserService().getCurrentUser());
        if (task == null) return;
        System.out.println("[ENTER NEW NAME]");
        String newName = bootstrap.getScanner().nextLine();
        if (newName == null || newName.isEmpty()) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        bootstrap.getTaskService().update(task, newName);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
