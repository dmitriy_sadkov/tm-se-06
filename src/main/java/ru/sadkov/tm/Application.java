package ru.sadkov.tm;

import ru.sadkov.tm.command.project.*;
import ru.sadkov.tm.command.task.*;
import ru.sadkov.tm.command.user.*;
import ru.sadkov.tm.launcher.Bootstrap;
import ru.sadkov.tm.command.system.HelpCommand;

public class Application {
    private final static Class[] classes = {HelpCommand.class, ProjectCreateCommand.class, ProjectFindAllCOmmand.class,
            ProjectFindOneCommand.class, ProjectRemoveCommand.class, ProjectRemoveAllCommand.class,
            ProjectUpdateCommand.class, TaskCreateCommand.class, TaskRemoveCommand.class, TaskRemoveAllCommand.class,
            TaskUpdateCommand.class, TaskFindOneCommand.class, TaskFindAllCommand.class, TaskForProjectCommand.class,
            UserAuthorizationCommand.class, UserEndSessionCommand.class, UserRegistryCommand.class, UserShowProfileCommand.class,
            UserUpdateInformationCommand.class, UserUpdatePasswordCommand.class};

    public static void main(String[] args) {
        new Bootstrap().init(classes);
    }
}

