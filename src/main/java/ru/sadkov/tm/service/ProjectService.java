package ru.sadkov.tm.service;

import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.repository.ProjectRepository;
import ru.sadkov.tm.util.RandomUUID;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProjectService {
    private ProjectRepository projectRepository = new ProjectRepository();
    private Scanner scanner;

    public ProjectService(Scanner scanner) {
        this.scanner = scanner;
    }

    public String findProjectIdByName(String projectName, User currentUser) {
        List<Project> projects = new ArrayList<>(projectRepository.findAll(currentUser).values());
        for (Project project : projects) {
            if (project.getName().equalsIgnoreCase(projectName)) {
                return project.getId();
            }
        }
        return null;
    }

    public void saveProject(String projectName, String userId) {
        projectRepository.merge(new Project(projectName, RandomUUID.genRandomUUID(), userId));
    }

    public void removeProject(String projectName, User currentUser) {
        String projectId = findProjectIdByName(projectName, currentUser);
        if (projectId == null || projectId.isEmpty()) return;
        Project project = projectRepository.findOne(projectId, currentUser);
        projectRepository.remove(projectName);
    }


    public void showProjectsForUser(User user) {
        List<Project> projects = new ArrayList<>(projectRepository.findAll(user).values());
        ListShowUtil.showList(projects);
    }

    public void clearProjects(User currentUser) {
        projectRepository.removeAll(currentUser);
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void update(Project project, String projectName, String description) {
        projectRepository.update(project,projectName,description);
    }

    public void showProject(String projectName, User currentUser) {
        String projectId = findProjectIdByName(projectName, currentUser);
        if (projectRepository.findAll(currentUser).containsKey(projectId)) {
            System.out.println(projectRepository.findOne(projectId, currentUser));
        }
    }
}
