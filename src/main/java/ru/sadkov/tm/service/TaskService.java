package ru.sadkov.tm.service;

import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.repository.TaskRepository;
import ru.sadkov.tm.util.RandomUUID;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class TaskService {
    private TaskRepository taskRepository = new TaskRepository();
    private ProjectService projectService;
    private Scanner scanner;

    public TaskService(ProjectService projectService, Scanner scanner) {
        this.projectService = projectService;
        this.scanner = scanner;
    }

    public TaskService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task findTaskByName(String taskName, User currentUser) {
        List<Task> tasks = new ArrayList<>(taskRepository.findAll(currentUser).values());
        for (Task task : tasks) {
            if (task.getName().equals(taskName)) {
                return task;
            }
        }
        return null;
    }

    public void saveTask(String taskName, String projectName, User currentUser) {
        String projectId = projectService.findProjectIdByName(projectName, currentUser);
        if (projectId.isEmpty()) {
            return;
        }
        taskRepository.merge(new Task(taskName, RandomUUID.genRandomUUID(), projectId, currentUser.getId()));
        System.out.println("[OK]");
    }

    public void removeTask(String taskName, User currentUser) {
        Task task = taskRepository.findTaskByName(taskName, currentUser);
        if (task == null) return;
        taskRepository.remove(taskName);
    }

    public void showTasks(User user) {
        if (taskRepository.findAll(user).isEmpty()) {
            System.out.println("[NO TASKS]");
            return;
        }
        List<Task> tasks = new ArrayList<>(taskRepository.findAll(user).values());
        ListShowUtil.showList(tasks);
    }


    public void clearTasks(User user) {
        taskRepository.removeAll(user);
    }

    public void removeTaskForProject(String projectName, User currentUser) {
        if (projectName == null || projectName.isEmpty() || currentUser == null) {
            return;
        }
        String projectId = projectService.findProjectIdByName(projectName,currentUser);
        Project project = projectService.getProjectRepository().findOne(projectId, currentUser);
        if (project.getUserId().equals(currentUser.getId())) {
            Iterator<Task> iterator = taskRepository.findAll(currentUser).values().iterator();
            while (iterator.hasNext()) {
                Task task = iterator.next();
                if (task.getProjectId().equals(projectId)) {
                    taskRepository.remove(task.getName());
                }
            }
        }
    }

    public void update(Task task, String taskName) {
        taskRepository.update(task,taskName);
    }

    public void showTask(String taskName, User user) {
        Task task = findTaskByName(taskName, user);
        if (task == null) {
            return;
        }
        System.out.println("[TASK INFORMATION]");
        System.out.println(task);
    }
}

