package ru.sadkov.tm.service;

import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.repository.UserRepository;
import ru.sadkov.tm.util.HashUtil;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

public class UserService {
    private UserRepository userRepository = new UserRepository();
    private User currentUser = null;

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public void userRegister(User user) throws NoSuchAlgorithmException {
        if (user == null) return;
        if (user.getLogin() == null || user.getLogin().isEmpty()) return;
        if (user.getPassword() == null || user.getPassword().isEmpty()) return;
        if (userRepository.findByLogin(user.getLogin()) != null) return;
        user.setPassword(HashUtil.hashMD5(user.getPassword()));
        userRepository.persist(user);
    }

    public boolean userAuthorize(String login, String password) throws NoSuchAlgorithmException {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        User user = userRepository.findByLogin(login);
        if (user == null) return false;
        if (user.getPassword().equals(HashUtil.hashMD5(password))) {
            currentUser = user;
            return true;
        }
        return false;
    }

    public void userLogout() {
        currentUser = null;
    }

    public void updatePassword(String newPassword) throws NoSuchAlgorithmException {
        if (newPassword == null || newPassword.isEmpty()) return;
        currentUser.setPassword(HashUtil.hashMD5(newPassword));
    }

    public User showUser() {
        return currentUser;
    }

    public boolean updateProfile(String newUserName) {
        if (newUserName == null || newUserName.isEmpty()) return false;
        currentUser.setLogin(newUserName);
        return true;
    }

    public void addTestUsers() throws NoSuchAlgorithmException {
        User admin = new User("admin", "admin", Role.ADMIN);
        User user = new User("user", "user", Role.USER);
        userRegister(admin);
        userRegister(user);
    }

    public boolean isAuth() {
        return currentUser != null;
    }
}
