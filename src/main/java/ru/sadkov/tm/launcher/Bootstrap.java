package ru.sadkov.tm.launcher;

import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.service.ProjectService;
import ru.sadkov.tm.service.TaskService;
import ru.sadkov.tm.service.UserService;
import java.util.*;

public class Bootstrap {
    private Scanner scanner = new Scanner(System.in);
    private ProjectService projectService = new ProjectService(scanner);
    private TaskService taskService = new TaskService(projectService, scanner);
    private UserService userService = new UserService();
    private Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public List<AbstractCommand> getCommandList() {
        return new ArrayList<>(commandMap.values());
    }

    public void setCommandMap(Map<String, AbstractCommand> commandMap) {
        this.commandMap = commandMap;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    private void registry(Class clazz) throws IllegalAccessException, InstantiationException {
        AbstractCommand abstractCommand = (AbstractCommand) clazz.newInstance();
        final String commandName = abstractCommand.command();
        final String commandDescription = abstractCommand.description();
        if (commandName == null || commandName.isEmpty()) return;
        if (commandDescription == null || commandDescription.isEmpty()) return;
        abstractCommand.setBootstrap(this);
        commandMap.put(commandName, abstractCommand);
    }

    private void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println("enter HELP for command list");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commandMap.get(command);
        if (abstractCommand == null) return;

        if (abstractCommand.safe() || (!abstractCommand.safe() && userService.isAuth())) {
            abstractCommand.execute();
            return;
        }
        System.out.println("[ACCESS DENIED]");
    }

    public void init(final Class... commandsClasses) {
        if (commandsClasses == null || commandsClasses.length == 0) return;
        try {
            for (Class clazz : commandsClasses) {
                registry(clazz);
            }
            userService.addTestUsers();
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
